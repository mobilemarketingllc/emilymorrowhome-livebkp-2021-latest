(function($) {
    // Site search dropdown
    $("#search-link").click(function() {
        $(".site-search").toggle();
        $(".site-search .search-field").focus();
        return false;
    });

    // Initialize featured content Swiper library
    var featured_content_swiper = new Swiper(".featured-content .swiper-container", {
        direction: "horizontal",
        loop: "true",
        nextButton: ".featured-content .swiper-button-next",
        prevButton: ".featured-content .swiper-button-prev",
        pagination: ".featured-content .swiper-pagination",
        paginationClickable: true
    });

    // Initialize featured collection Swiper library
    var windowWidth = $(window).width();
    if (windowWidth >= 768) {
        var slidesPerView = 3.5;
        if (windowWidth < 1024) {
            slidesPerView = 2.5;
        }
        var featured_collection_swiper = new Swiper(".featured-collections .swiper-container", {
            direction: "horizontal",
            loop: true,
            nextButton: ".featured-collections .swiper-button-next",
            prevButton: ".featured-collections .swiper-button-prev",
            slidesPerView: slidesPerView,
            centeredSlides: false,
            spaceBetween: 30
        });
    }

})(jQuery);

function product_docready($) {
    // Variable products - trigger a change
    $("#quantity-type").change();
}