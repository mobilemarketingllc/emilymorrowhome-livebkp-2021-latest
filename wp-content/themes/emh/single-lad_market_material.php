<?php
/**
 * The template for displaying the homepage.
 *
 * Template name: Homepage
 *
 * @package storefront
 */

get_header(); ?>



    <div id="primary" class="content-area">
        <main id="main" class="site-main home-site-main" role="main">

        <div class="pageTitleBackBtn">
            <div class="backBtn"><a href="/lad-marketing-materials/">Back</a></div>
             
        </div>
        <div class="filterRow">

        <h2>
            <?php 
            $term_obj_list = get_the_terms( $post->ID, 'lad_marketing_cat' );	
            echo $term_obj_list[0]->name
            ?>
            <?php
				$myposts = get_posts(array(
					'showposts' => -1,
					'post_type' => 'lad_market_material',
					'tax_query' => array(
						array(
						'taxonomy' => 'lad_marketing_cat',
						'field' => 'slug',
						'terms' => $term_obj_list[0]->slug)
					))
				);
             
			
			?>
         </h2>

			<div class="filterwrapper">
                <span class="filterdroptitle">FILTER BY</span>
                
                <select name="filterdrop" class="filterdrop" id="filterdrop">

                    <?php foreach ($myposts as $mypost) {				?>
                    <option value="<?php echo $mypost->ID ;?>" <?php if($mypost->ID == get_the_id()){ echo 'selected';} ?>>
                    <?php echo $mypost->post_title;?> </option>
                    <?php } ?>

                </select>
            </div>
        </div>

            <ul class="material_rooms imgDownloadPage">

                <?php while( have_rows('material_gallery') ): the_row(); 

					// vars
					$image = get_sub_field('upload_image');
                    $content = get_sub_field('image_title');
                    $download = get_sub_field('download_file');
 				?>

                <li class="">

                    <img src="<?php echo $image; ?>" alt="<?php echo $content; ?>" width="300" />

                    <a href="<?php echo $download; ?>" download>
                        <span class="mat_title"><?php echo $content; ?></span>
                        <img src="/wp-content/uploads/2018/12/download.png" width="24" />
                    </a>

                </li>

                <?php endwhile; ?>

                </ul>

<div class="clearfix"></div>
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

<script>

//Ajax Update/Adjust sale amount

jQuery("#filterdrop").live('change' ,function(e){     
        e.preventDefault();    

        var material_id = jQuery(this).val();   
		//alert(material_id);
       
        jQuery.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: {
			action : 'show_market_material',
            material_id : material_id 		
		},
            cache: false,
            success: function(data){  

				jQuery(".material_rooms").html(data);       
                
            }
        });
    });

        
        jQuery('body').append('<div class="imgPopup" style="display:none"></div>');

        jQuery('.imgDownloadPage>li>img').live('click',function(){
            jQuery('.imgPopup').css('display','flex');
            var thisImg = jQuery(this).attr('src');
            jQuery('.imgPopup').html('<div class="imgPopupInner"><img src="'+ thisImg +'" width="600"> <span class="closePop" onClick=jQuery(".imgPopup").toggle()>X</span></div>');
        })

        // var $imgPop = jQuery('.imgPopupInner, .imgDownloadPage>li>img');

        // jQuery(document).mouseup(function (e) {
        //     if (!$imgPop.is(e.target) && $imgPop.has(e.target).length === 0 && jQuery('.imgPopup').css('display') == 'flex') {
        //         // console.log((e.target) , $imgPop.has(e.target).length === 0 , jQuery('.imgPopup').css('display') == 'flex')
        //         jQuery('.imgPopup').toggle();
        //     }
        // });


</script>
    <?php
get_footer();