<?php
/**
 * The template for displaying the homepage.
 *
 * Template name: Homepage
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main home-site-main" role="main">
			<?php while (have_posts()) : the_post();
				do_action('storefront_page_before');
				get_template_part('content', 'home');
			endwhile; ?>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();