<?php
/**
 * The template for displaying the FCW Dealers' Choice Awards landing page.
 *
 * Template Name: FCW Dealers' Choice 
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post();
				do_action( 'storefront_page_before' );
			?>

				<div class="fcw-content" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>')">
					<section class="main-content">
					<h1><em>Hardwoo<span class="close">d.</span></em><br />Reimagine<span class="close">d.</span><br />Redefine<span class="close">d.</span><br />Reinvente<span class="close">d.</span></h1>
					<h2>Relax, you're finally home.</h2>
					<div class="fcw-badge">
						<div class="outer-border">
							<div class="middle-border">
								<div class="inner-border">
									<div class="badge-content">
										<p>
											<a href="https://1.shortstack.com/VG8jXX">Click here</a><br />to participate in our <br />Snap and Give Campaign!  

										</p>
										<p>
										#VoteEmily
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					</section>
					<section class="sub-content">
						<p>Get inspired with the new Emily Morrow Home Collection.</p>
						<p>Experience stunning hardwoods for the discerning luxury consumer.</p>
						<p><span class="subtext">Surfaces Booth #665</span></p>
					</section>
					<section class="footer-content">
						<p>Made in USA | Longer, Wider Planks | Handcrafted</p>
					</section>
				</div>

			<?php	do_action( 'storefront_page_after' ); 
			endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();