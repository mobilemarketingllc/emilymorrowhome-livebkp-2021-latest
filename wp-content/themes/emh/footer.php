<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

$facebook_url = theme_get_option("emh_facebook_url");
$twitter_url = theme_get_option("emh_twitter_url");
$pinterest_url = theme_get_option("emh_pinterest_url");
$instagram_url = theme_get_option("emh_instagram_url");
$linkedin_url = theme_get_option("emh_linkedin_url");
$googleplus_url = theme_get_option("emh_googleplus_url");
?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="social-footer" class="social-footer">
		<div class="col-full">
			<ul class="social-links">
				<?php if (!empty($facebook_url)) { ?>
					<li class="social-link facebook"><a href="<?php echo $facebook_url; ?>" target="_blank"><span class="fa fa-facebook" /></a></li>
				<?php } ?>
				<?php if (!empty($twitter_url)) { ?>
					<li class="social-link twitter"><a href="<?php echo $twitter_url; ?>" target="_blank"><span class="fa fa-twitter" /></a></li>
				<?php } ?>
				<?php if (!empty($pinterest_url)) { ?>
					<li class="social-link pinterest"><a href="<?php echo $pinterest_url; ?>" target="_blank"><span class="fa fa-pinterest" /></a></li>
				<?php } ?>
				<?php if (!empty($instagram_url)) { ?>
					<li class="social-link instagram"><a href="<?php echo $instagram_url; ?>" target="_blank"><span class="fa fa-instagram" /></a></li>
				<?php } ?>
				<?php if (!empty($linkedin_url)) { ?>
					<li class="social-link linkedin"><a href="<?php echo $linkedin_url; ?>" target="_blank"><span class="fa fa-linkedin" /></a></li>
				<?php } ?>
				<?php /*if (!empty($googleplus_url)) { ?>
					<li class="social-link googleplus"><a href="<?php echo $googleplus_url; ?>"><span class="fa fa-google" /></a></li>
				<?php }*/ ?>
			</ul>
		</div>
	</footer>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">
			<?php wp_nav_menu(array(
				"menu" => "Footer Menu"
			));
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 */
			do_action( 'storefront_footer' ); ?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>
<?php
global $product;

if( (has_term( 'flooring', 'product_cat', $product->ID ) && !is_user_logged_in() ) || (has_term( 'architectural-series-category', 'product_cat', $product->ID ) && !is_user_logged_in() ) || (has_term( 'flooring', 'product_cat', $product->ID ) && is_user_logged_in() ) || (has_term( 'architectural-series-category', 'product_cat', $product->ID ) && is_user_logged_in() )  ){
?>
<script type="text/javascript"> 
jQuery(document).ready(function(){
jQuery ( ".custom_dealerbtn" ).insertAfter( ".single-product .woocommerce-product-details__short-description" );
});
</script>
<?php } ?>
<!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59a47a4210bcb79a"></script> -->
</body>
</html>
