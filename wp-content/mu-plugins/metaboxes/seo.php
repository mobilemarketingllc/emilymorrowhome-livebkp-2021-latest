<?php

// Metaboxes to define meta descriptions
add_action('cmb2_init', 'seo_metaboxes');
function seo_metaboxes() {
    $prefix = 'emh_';
    $cmb_group = new_cmb2_box(array(
        'id' => $prefix . 'seo',
        'title' => __('SEO Options', 'storefront'),
        'object_types' => array('page', 'product'),
        'closed' => false
    ));

    $cmb_group->add_field(array(
        'id' => 'optimized_title',
        'name' => __('Optimized Title', 'storefront'),
        'type' => 'text'
    ));

    $cmb_group->add_field(array(
        'id' => 'meta_description',
        'name' => __('Meta Description', 'storefront'),
        'type' => 'textarea'
    ));

    $cmb_group = new_cmb2_box(array(
        'id' => $prefix . 'tax_seo',
        'title' => __('SEO Options', 'storefront'),
        'object_types' => array('term'),
        'taxonomies' => array('product_cat', 'lifestyles'),
        'closed' => false
    ));

    $cmb_group->add_field(array(
        'id' => 'optimized_title',
        'name' => __('Optimized Title', 'storefront'),
        'type' => 'text'
    ));

    $cmb_group->add_field(array(
        'id' => 'meta_description',
        'name' => __('Meta Description', 'storefront'),
        'type' => 'textarea'
    ));
}