<?php
/**
 * Admin section for managing featured products.
 */

class admin_featured_products {
    private $key = 'admin_featured_products';
    private $metabox_id = 'admin_featured_products_metabox';
    private $prefix = 'emh_featured_products_';
    protected $title = '';
    protected $featured_products_options_page = '';

    public function __construct() {
        $this->title = __('Featured Products', 'woocommerce');
    }

    public function hooks() {
        add_action('admin_init', array($this, 'init'));
        add_action('admin_menu', array($this, 'featured_products_admin_menu'));
        add_action('cmb2_init', array($this, 'add_featured_products_admin_metabox'));
    }

    public function init() {
        register_setting($this->key, $this->key);
    }

    public function featured_products_admin_menu() {
        $this->featured_products_options_page = add_menu_page(
            $this->title,
            $this->title,
            'manage_options',
            $this->key,
            array($this, 'featured_products_admin_render')
        );

        add_action("admin_print_styles-{$this->featured_products_options_page}", array($this, 'embed_css'));
    }

    public function featured_products_admin_render() {
        echo '<div class="wrap ' . $this->key . '">';
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->save_success_notice();
        }
        echo '<h1>' . get_admin_page_title() . '</h1>';
        cmb2_metabox_form($this->metabox_id, $this->key, array('cmb_styles' => false));
        echo '</div>';
    }

    public function add_featured_products_admin_metabox() {
        $cmb = new_cmb2_box(array(
            'id' => $this->metabox_id,
            'hookup' => false,
            'show_on' => array(
                'key' => 'options-page',
                'value' => array($this->key,)
            )
        ));

        // Categories
        $categories = [
            array(
                'name' => 'Flooring',
                'slug' => 'flooring'
            ),
            array(
                'name' => 'Furniture',
                'slug' => 'furniture'
            ),
            array(
                'name' => 'Home Décor',
                'slug' => 'home-decor'
            )
        ];

        foreach ($categories as $cat) {
            $title_field = $cmb->add_field(array(
                'name' => $cat['name'],
                'id' => $this->prefix . $cat['slug'],
                'type' => 'title'
            ));

            for ($i = 1; $i <= 6; $i++) {
                $cmb->add_field(array(
                    'name' => __('Product ' . $i, 'emh'),
                    'id' => $this->prefix . $cat['slug'] . '_' . $i,
                    'type' => 'select',
                    'show_names' => false,
                    'options_cb' => 'get_products_' . str_replace('-', '_', $cat['slug'])
                ));
            }
        }
    }

    public function embed_css() { ?>
        <style type="text/css">

        </style>
    <?php }

    function save_success_notice() { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e('Featured products saved.', 'emh'); ?></p>
        </div>
    <?php }
}

// This is stupid as you can't pass arguments to option_cb
function get_products_flooring($field) {
    return get_products('flooring', $field);
}

function get_products_furniture($field) {
    return get_products('furniture', $field);
}

function get_products_home_decor($field) {
    return get_products('home-decor', $field);
}

function get_products($category, $field) {
    $products = array();

    // Parent products
    $loop = new WP_Query(array(
        'post_type' => array('product'),
        'posts_per_page' => -1,
        'product_cat' => $category
    ));
    while ($loop->have_posts())
    {
        $loop->the_post();
        $theid = get_the_ID();
        $thetitle = get_the_title();
        $products[$theid] = $thetitle;
    }
    wp_reset_query();

    // Variant products
    $parents_to_delete = array();

    foreach ($products as $parent_id => $parent_title) {
        $loop = new WP_Query(array(
            'post_type' => array('product_variation'),
            'posts_per_page' => -1,
            'post_parent' => $parent_id
        ));
        if ($loop->have_posts()) {
            // Remove the parent
            $parents_to_delete[] = $parent_id;   
        }
        while ($loop->have_posts())
        {
            $loop->the_post();
            $theid = get_the_ID();
            $thetitle = $parent_title;

            // Concatenate variant values to make a more specific title
            $meta = get_post_meta($theid);
            $first = true;
            foreach ($meta as $meta_key => $meta_value) {
                if (substr($meta_key, 0, 9) === 'attribute') {
                    if ($first) {
                        $thetitle .= ' - ' . $meta_value[0];
                        $first = false;
                    } else {
                        $thetitle .= ', ' . $meta_value[0];
                    }
                }
            }

            $products[$theid] = $thetitle;
        }
    }

    // Delete unneeded parents of variable products
    foreach ($parents_to_delete as $parent_id) {
        unset($products[$parent_id]);
    }

    // Sort the products list by title
    asort($products);
    return $products;
}

function featured_products_admin() {
    $object = new admin_featured_products();
    $object->hooks();
    return $object;
}

featured_products_admin();
?>